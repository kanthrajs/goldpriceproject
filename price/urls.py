# blog/urls.py
from django.urls import path
from django.conf.urls import url
from price import views
from .views import PriceDetailView
from django.contrib.sitemaps.views import sitemap
from .sitemaps import GoldPriceSitemap

sitemaps = {
    'slug': GoldPriceSitemap
}

urlpatterns = [
url(r'^sitemap\.xml/$', sitemap, {'sitemaps' : sitemaps } , name='sitemap'),
path('', views.Index, name='home'),
path('gold-price-today/<slug:slug>', views.PriceDetailView, name='PriceDetailView'),
path('gold-price-today/<slug:slug>/', views.PriceDetailView, name='PriceDetailView'),
]
