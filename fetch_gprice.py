import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'goldprice_project.settings')

import django
django.setup()

import requests
from bs4 import BeautifulSoup
from price.models import GoldPrice
import datetime
from django.utils import timezone



test_cities = ['/','agra', 'ahmedabad', 'aligarh', 'allahabad', 'ambala', 'amravati', 'aurangabad', 'bangalore', 'bareilly',
'belgaum', 'bellary', 'bhilai', 'bhopal', 'bhubaneshwar', 'chandigarh', 'chennai', 'coimbatore', 'cuttack',
'dehradun', 'delhi', 'dindigul', 'erode', 'faridabad', 'ghaziabad', 'gulbarga', 'guntur', 'gurgaon', 'guwahati',
'haldwani', 'hisar', 'hosur', 'hyderabad', 'indore', 'jabalpur', 'jaipur', 'jalgaon', 'jammu', 'jamnagar',
'jamshedpur', 'jodhpur', 'kakinada', 'kannur', 'kanpur', 'karnal', 'karur', 'kochi', 'kolhapur', 'kolkata',
'kota', 'kozhikode', 'kurnool', 'lucknow', 'ludhiana', 'madurai', 'malappuram', 'mangalore', 'meerut', 'mumbai',
'mysore', 'nagercoil', 'nagpur', 'nanded', 'nasik', 'nellore', 'noida', 'panaji', 'patna', 'pondicherry',
 'proddatur', 'pune', 'raipur', 'rajahmundry', 'rajkot', 'ranchi', 'ratlam', 'salem', 'sangli',
 'shimla', 'shimoga', 'siliguri', 'solapur', 'srinagar', 'surat', 'thane', 'thanjavur', 'thiruvananthapuram',
  'thrissur', 'tiruchirapalli', 'tirunelveli', 'tuticorin', 'udaipur', 'udupi', 'vadodara', 'varanasi',
  'vellore', 'vijayawada', 'visakhapatnam', 'warangal', 'kerala']
test_citiesq = ['/','kakinada']
def start_fetching():
    if check_before_fetch():
        filtered_data_array = []
        for test_city in test_cities:
            if test_city == '/':
                prepared_link = "https://www.goldpriceindia.com/"
            else:
                prepared_link = "https://www.goldpriceindia.com/gold-price-" + test_city + ".php"
            print(prepared_link)
            # get data from request
            filtered_data = fetch_gprice(prepared_link, test_city)
            filtered_data_array.append(filtered_data)
        print(filtered_data_array)
            # save to database
        for data in filtered_data_array:
            save_to_database(data)
    else:
        print("Market NOT Open")

def check_before_fetch():
    now = datetime.datetime.now()
    now = datetime.time(now.hour, now.minute)
    #print("Current time",now)

    start_time =  datetime.time(9,45)
    end_time = datetime.time(22,30)
    #print("this is start time",start_time)
    #print("this is start time",end_time)

    if (start_time < now < end_time ):
        return True
    else:
        return False

def fetch_gprice(prepared_link, test_city):
    global status
    res = requests.get(prepared_link)
    unfiltered_data = res.text
    soup = BeautifulSoup(unfiltered_data, 'html.parser')
    if soup.find("span", class_="txt-low"):
        fetched_gprice = soup.find_all("span", class_="txt-low")
        status = False
        price_string = soup.select("span.txt-low")[0].text
        price_string = price_string[:-1]
        ChangePercentage = soup.select("span.txt-low")[4].text
        ChangePercentage = ChangePercentage.replace('&percnt','%')
    elif soup.find("span", class_="txt-neutral"):
        fetched_gprice = soup.find_all("span", class_="txt-neutral")
        status = None
        price_string = soup.select("span.txt-neutral")[0].text
        ChangePercentage = "0.00"
    else:
        etched_gprice = soup.find_all("span", class_="txt-high")
        status = True
        price_string = soup.select("span.txt-high")[0].text
        price_string = price_string[:-1]
        ChangePercentage = soup.select("span.txt-high")[4].text
        ChangePercentage = ChangePercentage.replace('&percnt','%')
    price_int = int(price_string[1:3] + price_string[-3:])

    if test_city == "/":
        test_city = "India"
        price_int_22k = 0
    else:
        fetched_gprice_22k = soup.find_all("td", class_="pl-5")
        price_string_22k = soup.select("td.pl-5")[1].text
        #print(price_string_22k)
        price_string_22k = price_string_22k[:7]
        price_int_22k = int(price_string_22k[1:3] + price_string_22k[-3:])


    now = datetime.datetime.now()
    filtered_data = {"Date":now.strftime("%Y-%m-%d"),"City": test_city.capitalize(),"GPrice":price_int,"22k":price_int_22k,"Status":status,"ChangePercentage":ChangePercentage}
    print(filtered_data["Date"])
    return filtered_data

def save_to_database(data):
    c = GoldPrice.objects.get_or_create(City=data["City"])[0]
    #today = timezone.now().date() - timedelta(days=1)
    #print("this is acutal date {}".format(today))
    #print("this is reslt date {}".format((c.Date) - timedelta(days=1)))

    if c.GoldPrice != data["GPrice"]:
    #c.Date = data["Date"]
        print('New Gold Price Right Now')
        c.DateTimeFromEdited = timezone.now()
        c.Status = data["Status"]
        c.City = data["City"]
        c.ChangePercentage = data["ChangePercentage"]
        c.GoldPrice = data["GPrice"]
        c.TwoFourK = data["GPrice"]
        c.TwoTwoK = data["22k"]
        c.save()
    else:
        print("No Change in Gold PRice")
        c.DateTimeFromEdited = timezone.now()
        c.save()
    return c

# Start execution here
if __name__ == '__main__':
    print("Starting Gprice population script...")
    start_fetching()
