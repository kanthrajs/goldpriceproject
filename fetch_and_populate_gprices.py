import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'goldprice_project.settings')

import django
django.setup()

from price.models import Category

cities = ['agra', 'ahmedabad', 'aligarh', 'allahabad', 'ambala', 'amravati', 'aurangabad', 'bangalore', 'bareilly',
'belgaum', 'bellary', 'bhilai', 'bhopal', 'bhubaneshwar', 'chandigarh', 'chennai', 'coimbatore', 'cuttack',
'dehradun', 'delhi', 'dindigul', 'erode', 'faridabad', 'ghaziabad', 'gulbarga', 'guntur', 'gurgaon', 'guwahati',
'haldwani', 'hisar', 'hosur', 'hyderabad', 'indore', 'jabalpur', 'jaipur', 'jalgaon', 'jammu', 'jamnagar',
'jamshedpur', 'jodhpur', 'kakinada', 'kannur', 'kanpur', 'karnal', 'karur', 'kochi', 'kolhapur', 'kolkata',
'kota', 'kozhikode', 'kurnool', 'lucknow', 'ludhiana', 'madurai', 'malappuram', 'mangalore', 'meerut', 'mumbai',
'mysore', 'nagercoil', 'nagpur', 'nanded', 'nasik', 'nellore', 'noida', 'panaji', 'patna', 'pondicherry',
 'proddatur', 'pune', 'raipur', 'rajahmundry', 'rajkot', 'ranchi', 'ratlam', 'salem', 'sangli',
 'shimla', 'shimoga', 'siliguri', 'solapur', 'srinagar', 'surat', 'thane', 'thanjavur', 'thiruvananthapuram',
  'thrissur', 'tiruchirapalli', 'tirunelveli', 'tuticorin', 'udaipur', 'udupi', 'vadodara', 'varanasi',
  'vellore', 'vijayawada', 'visakhapatnam', 'warangal', 'kerala']

test_cities = ['mumbai','bangalore']
for test_city in test_cities:



def populate():
    for categry, cat_data in categories.items():
        c = add_cat(categry, cat_data["views"], cat_data["likes"])
        for p in cat_data["pages"]:
            add_page(c, p["title"], p["url"], p["views"])

    # Print out what we have added to the user.
    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print("- {0} - {1}".format(str(c), str(p)))

def add_page(categry, title, url, views=0):
    p = Page.objects.get_or_create(category=categry, title=title)[0]
    p.url=url
    p.views=views
    # we need to save the changes we made!!
    p.save()
    return p

def add_cat(name, views=0, likes=0):
    c = Category.objects.get_or_create(name=name)[0]
    c.views=views
    c.likes=likes
    c.save()
    return c

# Start execution here!
if __name__ == '__main__':
    print("Starting Rango population script...")
    populate()
