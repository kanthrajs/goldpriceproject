from django.shortcuts import render
from django.views.generic import ListView, DetailView

from .models import GoldPrice
# Create your views here.
def Index(request):
    index = GoldPrice.objects.get(City="India")
    index = index.__dict__
    #main_dict = {'mdata':index}
    #print("this is index fetchd from DB", index)
    #print(type(index))
    all_cities = GoldPrice.objects.values_list('City', flat=True)
    #print(all_cities)
    all_cities = list(all_cities)

    editGP = '{:,}'.format(index["GoldPrice"])
    #editPP = '{:,}'.format(index["PreviousPrice"])
    #index["PreviousPrice"] = editPP
    index["GoldPrice"] = editGP

    return render(request, 'gprice.html', {"object":index,"cities":all_cities})


def PriceDetailView(request, slug):
    city = slug.capitalize()
    if city == 'Cities':
        all_cities = GoldPrice.objects.values_list('City', flat=True)
        #print(all_cities)
        all_cities = list(all_cities)

        #print(all_cities)
        return render(request, 'asd.html', {"cities":all_cities})
    else:
        all_cities = GoldPrice.objects.values_list('City', flat=True)
        #print(all_cities)
        all_cities = list(all_cities)
        city_data = GoldPrice.objects.get(City=city)
        city_data = city_data.__dict__
        editGP = '{:,}'.format(city_data["GoldPrice"])
        #editPP = '{:,}'.format(city_data["PreviousPrice"])
        edit22k = '{:,}'.format(city_data["TwoTwoK"])
        city_data["GoldPrice"] = editGP
        #city_data["PreviousPrice"] = editPP
        city_data["TwoTwoK"] = edit22k
        return render(request, 'gprice.html', {"object":city_data,"cities":all_cities})
