from django.db import models
from django.utils import timezone
from django.urls import reverse
# Create your models here.
class GoldPrice(models.Model):
    DateTime = models.DateTimeField(auto_now_add=True)
    DateTimeModified = models.DateTimeField(auto_now=True)
    DateTimeFromEdited = models.DateTimeField(default=timezone.now)
    City = models.CharField(max_length=20)
    GoldPrice = models.PositiveIntegerField(null=True)
    LOCATOR_YES_NO_CHOICES_y = ((None,'0'), (True,'High'), (False, 'Low'))
    Status = models.NullBooleanField(choices=LOCATOR_YES_NO_CHOICES_y,
                                max_length=3,
                                blank=True, null=True, default=None,)
    ChangePercentage = models.CharField(max_length=15, null=True)
    TwoFourK = models.PositiveIntegerField(null=True)
    TwoTwoK = models.PositiveIntegerField(null=True)

    def __str__(self):
        return self.City

    def get_absolute_url(self):
        if self.City == "India":
            return reverse('home')
        else:
            return reverse('PriceDetailView', kwargs={"slug": self.City})
