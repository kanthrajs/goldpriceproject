import requests
from bs4 import BeautifulSoup

res = requests.get('https://www.goldpriceindia.com/gold-price-in-cities.php')
data = res.text

soup = BeautifulSoup(data, 'html.parser')
#print(soup.prettify())
cities = []
clean_cities = []
for links in soup.find_all('a'):
    if links.get('href').startswith('gold-price'):
        cities.append(links.get('href'))
#print(cities)

for city in cities:
    clean_cities.append(city[11:-4])

print(clean_cities)
print("This is the total cities {}".format(len(clean_cities)))
