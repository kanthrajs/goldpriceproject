from django.contrib.sitemaps import Sitemap
from .models import GoldPrice

class GoldPriceSitemap(Sitemap):
    changefreq = "hourly"
    priority = 0.8
    protocol = "https"

    def items(self):
        return GoldPrice.objects.all()

    def lastmod(self, obj):
        return obj.DateTimeFromEdited
