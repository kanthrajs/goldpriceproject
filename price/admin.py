from django.contrib import admin
from .models import GoldPrice
# Register your models here.
class PriceAdmin(admin.ModelAdmin):
    readonly_fields = ('DateTime','DateTimeModified')
    list_display = ('DateTimeFromEdited','ChangePercentage','City', 'GoldPrice', 'TwoFourK', 'TwoTwoK','Status')
admin.site.register(GoldPrice, PriceAdmin)
