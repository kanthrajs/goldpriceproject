# Generated by Django 2.1 on 2018-11-28 05:54

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('price', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='GoldPrice',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('DateTime', models.DateTimeField(auto_now_add=True)),
                ('DateTimeModified', models.DateTimeField(auto_now=True)),
                ('DateTimeFromEdited', models.DateTimeField(default=django.utils.timezone.now)),
                ('City', models.CharField(max_length=20)),
                ('GoldPrice', models.PositiveIntegerField(null=True)),
                ('Status', models.NullBooleanField(choices=[(None, ''), (True, 'High'), (False, 'Low')], default=None, max_length=3)),
                ('PreviousPrice', models.PositiveIntegerField(null=True)),
                ('TwoFourK', models.PositiveIntegerField(null=True)),
                ('TwoTwoK', models.PositiveIntegerField(null=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Price',
        ),
    ]
